(function() {
  'use strict';
  angular
    .module('logoApp', ['angularMoment', 'ui.bootstrap'])
    .controller('MainController', MainController)
  ;

  function MainController() {
    var vm = this;

    vm.init = function() {
      vm.child = {
        birth_date: moment().subtract(9, 'years').toDate(),
        name:''
      };
      vm.evaluation_date = new Date();
      vm.dateDiff = null;
      vm.changeDate();
      vm.belo = angular.copy(belo); // belo.js
      vm.elo = angular.copy(elo); // elo.js
      vm.level = '1_cp';
      vm.hide_unused_proofs = true;
      vm.active = 'belo';
    };

    vm.getTableData = function(testName) {
      var testType = vm[testName];
      var result = [['Epreuve', 'Score Brut', 'Percentile']];
      if(testName === 'elo') {
        result[0].push('Ecart Type')
      }
      angular.forEach(testType.proofs, function(proof) {
        var score = proof.score || '';
        if(! testType.norms[proof.ref][vm.level]) {
          return
        }
        var row = [proof.label, proof.score, testType.norms[proof.ref][vm.level][proof.score]];
        if(testName === 'elo') {
          var stdDeviation = '';
          var deviationVariables = testType.std_dev[proof.ref][vm.level];
          if(deviationVariables) {
            if(proof.order == 'descending') {
              stdDeviation = (deviationVariables.avg - proof.score ) / deviationVariables.dev;
            } else {
              stdDeviation = (proof.score - deviationVariables.avg ) / deviationVariables.dev;
            }
          }
          row.push(stdDeviation || '')
        }
        result.push(row);
      });
      return result;
    };

    vm.changeDate = function() {
      vm.dateDiff =  moment.preciseDiff(vm.child.birth_date, vm.evaluation_date, true);
    }

    vm.export = function() {
      var data = vm.getTableData(vm.active);
      var csvContent = "data:text/csv;charset=utf-8,";

      data.forEach(function(infoArray, index){

         var dataString = infoArray.join(",");
         csvContent += index < data.length ? dataString+ "\n" : dataString;

      });
      var encodedUri = encodeURI(csvContent);
      // create a link to download the file with filename
      var link = document.createElement('a');
      link.className  ='dllink';
      console.log(link, link.className);
      link.download = vm.active + '-' + vm.child.name;
      link.href = encodedUri;
      document.body.appendChild(link);
      window.setTimeout(function () {
        document.body.removeChild(link);
      }, 300);
      link.click();
    };

    vm.init();
  };

})();
