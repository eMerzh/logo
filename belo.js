var proofs = [
  {label: "Graphèmes /26", max: 26, ref: 'grapheme'},
  {label: "Syllabes simples /11", max: 11, ref: 'syllabe_simple'},
  {label: "Syllabes complexes /15", max: 15, ref: 'syllabe_complex'},
  {label: "Graphies complexes /33", max: 33, ref: 'graphies_complexe'},
  {label: "Variation de la prononciation /15", max: 15, ref: 'prononciation'},
  {label: "Phonologie - Répétition : mots /8", max: 8, ref: 'repetition'},
  {label: "Répétition : chiffres endroit /5", max: 5, ref: 'repetition_endroit'},
  {label: "Répétition : chiffres envers /4", max: 4, ref: 'repetition_envers'},
  {label: "Lectures mots réguliers : Exactitude /12", max: 12, ref: 'lecture_reg_exact'},
  {label: "Lectures mots réguliers : Définition /12", max: 12, ref: 'lecture_reg_definition'},
  {label: "Lectures mots réguliers : Temps (max 133)", max: 133, ref: 'lecture_reg_temps'},
  {label: "Lectures mots irréguliers : Exactitude /12", max: 12, ref: 'lecture_irreg_exact'},
  {label: "Lectures mots irréguliers : Définition /12", max: 12, ref: 'lecture_irreg_definition'},
  {label: "Lectures mots irréguliers : Temps (max 150)", max: 150, ref: 'lecture_irreg_temps'},
  {label: "Dénomination rapide : nombre d'erreurs (max 4)", max: 4, ref: 'denom_rapide_erreur'},
  {label: "Dénomination rapide : temps (max 31)", max: 31, ref: 'denom_rapide_temps'},
  {label: "Test visuo-attentionnel : total /6", max: 6, ref: 'visuo_attention'},
  {label: "Conscience phonologique : total /16", max: 16, ref: 'conscience_phono'},
  {label: "Orthographe : syllabes /10", max: 10, ref: 'ortho_syllabe'},
  {label: "Orthographe : mots : total /15", max: 15, ref: 'ortho_mot'},
  {label: "Orthographe : phrases : total /35", max: 35, ref: 'ortho_phrase'},
  {label: "Lecture texte : nombre d'erreurs", max: 32, ref: 'lecture_text_exact'},
  {label: "Lecture texte : temps (max 128)", max: 128, ref: 'lecture_text_temps'},
  {label: "Lecture texte : qualité /6", max: 6, ref: 'lecture_text_qualite'},
  {label: "Lecture texte : compréhension /10", max: 10, ref: 'lecture_text_comprehension'},
];

var norms =
{
  denom_rapide_erreur: {
    '1_cp': [100, 25,  10,   2, 1,],
    '2_cp': [100, 10,   3, 0.3, 0.3,],
    '3_cp': [100,  8, 0.3, 0.3, 0.3,],
    'ce1':  [100,  5, 0.3, 0.3, 0.3,],
  },
  repetition: {
    '1_cp': [0.3, 0.3, 0.3,   1,   4,   5,  15, 37, 100,],
    '2_cp': [0.3, 0.3,   1,   3,   3,   3,  10, 17, 100,],
    '3_cp': [0.3, 0.3, 0.3, 0.3, 0.3, 0.3,   1, 20, 100,],
    'ce1':  [0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3,  9, 100,],
  },
  syllabe_simple: {
    '1_cp': [  3,   4,   8,  12,  13,  16,  23,  34,  45,  61,  80, 100,],
    '2_cp': [  1,   1,   2,   3,   3,   4,   6,  10,  23,  34,  54, 100,],
    '3_cp': [0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3,   1,   2,   9,  28, 100,],
    'ce1':  [0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3,   9, 100,],
  },
  syllabe_complex: {
    '1_cp': [ 9, 14, 17, 22, 25, 31, 43, 46, 51, 59, 68, 13, 78, 80, 88, 100, ],
    '2_cp': [ 3, 6, 7, 9, 11, 13, 16, 20, 21, 27, 34, 40, 50, 70, 75, 100,],
    '3_cp': [ 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 2, 5, 6, 11, 16, 20, 31, 56, 100,],
    'ce1':  [0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 1, 2, 4, 13, 31, 100,],
  },
  graphies_complexe: {
    '1_cp': [1, 3, 4, 6, 11, 12, 18, 22, 27, 36, 43, 53, 56, 62, 67, 68, 73, 76, 78, 85, 86, 87, 88, 93, 95, 96, 97, 99, 99, 99, 100, 100, 100, 100, ],
    '2_cp': [0.3, 0.3, 0.3, 0.3, 0.3, 3, 7, 9, 9, 9, 10, 10, 11, 13, 17, 20, 21, 27, 33, 37, 43, 53, 60, 73, 79, 81, 87, 90, 93, 97, 100, 100, 100, 100, ],
    '3_cp': [0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 1, 2, 2, 2, 2, 3, 3, 5, 7, 9, 14, 19, 24, 36, 39, 46, 62, 70, 82, 92, 98, 100, 100,],
    'ce1':  [0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 1, 1, 1, 1, 1, 2, 5, 8, 15, 23, 42, 57, 67, 84, 94, 99, 100, ],
  },
   grapheme: {
    '1_cp': [0.3, 0.3, 1, 1, 1, 1, 2, 2, 2, 2, 2, 3, 4, 5, 5, 6, 7, 11, 12, 13, 16, 23, 36, 41, 65, 84, 100, ],
    '2_cp': [0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 1, 3, 7, 16, 20, 40, 63, 79, 100,],
    '3_cp': [0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 1, 3, 5, 14, 32, 69, 100,],
    'ce1':  [0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 1, 2, 8, 22, 47, 100,],
  },
  prononciation: {
    '1_cp': [6, 9, 11, 21, 26, 42, 55, 66, 75, 85, 94, 96, 98, 99, 100, 100, ],
    '2_cp': [1, 1, 3, 4, 6, 16, 21, 26, 39, 44, 53, 66, 77, 90, 93, 100, ],
    '3_cp': [0.3, 0.3, 0.3, 0.3, 0.3, 1, 4, 7, 10, 21, 31, 55, 78, 93, 99, 100,],
    'ce1':  [0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 1, 5, 16, 44, 78, 97, 100, ],
  },
  repetition_endroit: {
    '1_cp': [7, 12, 31, 63, 93, 100, ],
    '2_cp': [6, 13, 29, 60, 89, 100, ],
    '3_cp': [5, 13, 27, 59, 90, 100, ],
    'ce1':  [0.3, 4, 10, 31, 79, 100,],
  },
 repetition_envers: {
    '1_cp': [4, 19, 41, 77, 100,],
    '2_cp': [4, 31, 67, 99, 100,],
    '3_cp': [0.3, 1, 30, 72, 100, ],
    'ce1':  [0.3, 1, 17, 55, 100, ],
  },
  lecture_reg_exact: {
    '1_cp': [28, 38, 52, 62, 72, 86, 86, 87, 88, 89, 90, 93, 100, ],
    '2_cp': [1, 5, 9, 10, 16, 19, 26, 34, 41, 46, 63, 77, 100, ],
    '3_cp': [0.3, 0.3, 0.3, 0.3, 0.3, 1, 2, 3, 4, 9, 22, 52, 100,],
    'ce1':  [0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 1, 2, 5, 23.5, 100,],
  },
  lecture_reg_definition: {
    '1_cp': [31, 38, 52, 66, 76, 86, 87, 88, 89, 90, 94, 97, 100,],
    '2_cp': [1, 4, 9, 14, 20, 23, 30, 37, 47, 56, 71, 83, 100,],
    '3_cp': [0.3, 0.3, 0.3, 0.3, 0.3, 1, 3, 4, 13, 30, 54, 76, 100,],
    'ce1':  [0.3, 0.3, 0.3, 0.3, 0.3, 1, 2, 3, 7, 14, 22.5, 52, 100,],
  },
  lecture_reg_temps: {
    '1_cp': null,
    '2_cp': [0, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 75, 75, 75, 75, 75, 75, 75, 75, 75, 75, 75, 75, 75, 75, 75, 75, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 5, ],
    '3_cp': [0, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 75, 75, 75, 75, 75, 75, 75, 50, 50, 50, 50, 50, 50, 50, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, ],
    'ce1':  [0, 90, 90, 90, 90, 90, 90, 75, 75, 75, 50, 50, 50, 50, 25, 25, 25, 25, 10, 10, 10, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5,],
  },
  lecture_irreg_exact: {
    '1_cp': [34.5, 55, 76, 83, 86, 87, 88, 90, 93, 97, 98, 99, 100,],
    '2_cp': [1, 9, 13, 20, 29, 40, 56, 67, 77, 79, 86, 93, 100,],
    '3_cp': [0.3, 0.3, 0.3, 1, 2, 5, 10, 19, 35, 50, 73, 90, 100],
    'ce1':  [0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 1, 2, 5, 10, 22.5, 55, 100, ],
  },
  lecture_irreg_definition: {
    '1_cp': [38, 62, 76, 83, 86, 88, 90, 93, 95, 97, 100, 100, 100,],
    '2_cp': [3, 10, 16, 26, 39, 51, 60, 70, 79, 86, 93, 97, 100,],
    '3_cp': [0.3, 0.3, 0.3, 1, 6, 12, 27, 40, 54, 74, 91, 95, 100,],
    'ce1':  [0.3, 0.3, 0.3, 0.3, 2, 3, 4, 10, 20, 35, 54, 80, 100, ],
  },
  lecture_irreg_temps: {
    '1_cp': null,
    '2_cp': [90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 75, 75, 75, 75, 75, 75, 75, 75, 75, 75, 75, 75, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 5, ],
    '3_cp': [90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 75, 75, 75, 75, 75, 50, 50, 50, 50, 50, 50, 50, 50, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 10, 10, 10, 10, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5,],
    'ce1':  [90, 90, 90, 90, 90, 90, 90, 75, 75, 50, 50, 50, 50, 50, 25, 25, 25, 25, 25, 25, 10, 10, 10, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, ],
  },
  denom_rapide_temps: {
    '1_cp': [100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 75, 50, 50, 50, 50, 25, 25, 25, 25, 10, 10, 10, 10, 10, 5,],
    '2_cp': [100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 75, 75, 50, 50, 50, 50, 50, 50, 25, 25, 25, 25, 25, 25, 10, 5,],
    '3_cp': [100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 75, 75, 50, 50, 50, 25, 25, 25, 25, 10, 10, 10, 10, 10, 5, 5,],
    'ce1':  [100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 75, 75, 50, 50, 50, 25, 25, 25, 25, 10, 5, 5, 5, 5, 5, 5, 5, ],
  },
  visuo_attention: {
    '1_cp': [5, 5, 5, 5, 10, 25, 50,],
    '2_cp': [5, 5, 5, 5, 10, 25, 50,],
    '3_cp': [5, 5, 5, 5, 5, 10, 50,],
    'ce1':  [5, 5, 5, 5, 5, 10, 25,],
  },
  conscience_phono: {
    '1_cp': [5, 5, 5, 5, 5, 5, 5, 10, 10, 10, 25, 25, 25, 50, 50, 75, 100, ],
    '2_cp': [5, 5, 5, 5, 5, 5, 5, 5, 10, 10, 10, 10, 25, 25, 50, 75, 100, ],
    '3_cp': [5, 5, 5, 10, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 50, 75, 100, ],
    'ce1':  [5, 5, 5, 5, 5, 5, 10, 10, 25, 25, 50, 50, 50, 75, 75, 100, 100, ],
  },
  ortho_syllabe: {
    '1_cp': [6, 17, 24, 29, 36, 50, 60, 67, 82, 92, 100,],
    '2_cp': [6, 9, 10, 13, 16, 23, 37, 43, 57, 79, 100,],
    '3_cp': [0.3, 0.3, 0.3, 0.3, 2, 7, 12, 20, 33, 56, 100,],
    'ce1':  [0.3, 0.3, 0.3, 0.3, 1, 2, 3, 5, 16, 38, 100, ],
  },
  ortho_mot: {
    '1_cp': null,
    '2_cp': [6, 9, 17, 30, 33, 40, 49, 51, 60, 71, 84, 89, 93, 97, 98, 100,],
    '3_cp': [1, 2, 3, 4, 8, 15, 18, 28, 33, 42, 55, 68, 88, 96, 99, 100,],
    'ce1':  [0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 1, 1, 2, 3, 6, 11, 25, 57, 85, 100,],
  },
  ortho_phrase: {
    '1_cp': null,
    '2_cp': null,
    '3_cp': [0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 1, 2, 4, 6, 9, 13, 18, 19, 22, 28, 35, 40, 50, 62, 72, 76, 83, 91, 97, 100, 100, 100, 100, 100, 100,],
    'ce1':  [0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 1, 1, 1, 1, 1, 1, 1, 1, 2, 5, 6, 7, 12, 13, 21, 26, 32, 49, 63, 74, 84, 95, 98, 100, ],
  },
  lecture_text_exact: {
    '1_cp': null,
    '2_cp': null,
    '3_cp': [100, 73, 51, 33, 20, 10, 7, 4, 4, 4, 3, 3, 3, 3, 3, 3, 3, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1,],
    'ce1':  [50, 25, 10, 7, 3, 1, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3,],
  },
  lecture_text_temps: {
    '1_cp': null,
    '2_cp': null,
    '3_cp': [90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 75, 75, 75, 75, 75, 75, 75, 75, 75, 75, 75, 75, 75, 75, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 5,],
    'ce1':  [90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 75, 75, 75, 75, 75, 75, 75, 75, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, ],
  },
 lecture_text_qualite: {
    '1_cp': null,
    '2_cp': null,
    '3_cp': [1, 5, 18, 44, 62, 81, 100,],
    'ce1':  [2, 5, 9, 17, 33, 78, 100,],
  },
  lecture_text_comprehension: {
    '1_cp': null,
    '2_cp': null,
    '3_cp': [1, 3, 5, 6, 9, 18, 29, 50, 72, 92, 100,],
    'ce1':  [1, 3, 4, 6, 7, 11, 16, 36, 58, 81, 100,],
  },
}


var belo = {
  levels: {
    '1_cp': '1er trimestre CP',
    '2_cp': '2e trimestre CP',
    '3_cp': '3e trimestre CP',
    'ce1': 'CE1',
  },
  proofs: proofs,
  norms: norms,
};
